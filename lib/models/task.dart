import 'package:flutter/widgets.dart';

import 'task_exeption.dart';

enum TaskStatus {
  TO_DO,
  IN_PROGRESS,
  DONE,
}

@immutable
class Task {
  final String id;
  final String title;
  final String description;
  final String? userId;
  final TaskStatus status;

  Task({
    required this.id,
    required this.title,
    required this.description,
    this.userId,
    this.status = TaskStatus.TO_DO,
  });

  Task copyWith({
    String? title,
    String? description,
    String? userId,
    TaskStatus? status,
  }) =>
      Task(
        id: this.id,
        title: title ?? this.title,
        description: description ?? this.description,
        userId: userId ?? this.userId,
        status: status ?? this.status,
      );

  factory Task.fromMap(Map<String, dynamic> map) {
    try {
      return Task(
        id: map['id'],
        title: map['title'],
        description: map['description'],
        status: TaskStatus.values[map['status']],
        userId: map['userId'],
      );
    } catch (e, s) {
      throw TaskExeption(message: 'Error while parsing taks', stackTrace: s);
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'description': this.description,
      'title': this.title,
      'status': this.status.index,
      'userId': this.userId,
    };
  }
}
