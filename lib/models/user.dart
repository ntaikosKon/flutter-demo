import 'user_exeption.dart';

enum UserRole {
  DEVELOPER,
  DESIGNER,
}

class User {
  final String id;
  final String name;
  final UserRole role;

  User({
    required this.id,
    required this.name,
    required this.role,
  });

  User copyWith({
    String? name,
    UserRole? role,
  }) =>
      User(
        id: this.id,
        name: name ?? this.name,
        role: role ?? this.role,
      );

  factory User.fromMap(Map<String, dynamic> map) {
    try {
      return User(
        id: map['id'],
        name: map['name'],
        role: UserRole.values[map['role']],
      );
    } catch (e, s) {
      throw UserException(message: 'Error while parsing taks', stackTrace: s);
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'name': this.name,
      'role': this.role.index,
    };
  }
}
