class TaskExeption implements Exception {
  final String message;
  final StackTrace? stackTrace;

  TaskExeption({required this.message, this.stackTrace});

  @override
  String toString() {
    return message;
  }
}
