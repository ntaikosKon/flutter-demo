class UserException implements Exception {
  final String message;
  final StackTrace? stackTrace;

  UserException({required this.message, this.stackTrace});

  @override
  String toString() {
    return message;
  }
}
