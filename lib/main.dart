import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:fluttertoast/fluttertoast.dart';

// import 'package:demo/repositories/fake_task_repository.dart';
// import 'package:demo/repositories/fake_users_repository.dart';

import 'screens/screens.dart';
import 'providers/providers.dart';
import 'router.dart';
import 'shared/app_colors.dart';
import 'shared/route_transitions.dart';
import 'widgets/widgets.dart';
import 'extentions/extention_navigator_state.dart';

///
// Uncomment the following overrides to use Fake Repositories instead of the
// the real ones
//
// You can modify the Fake Repositories in order to test the behavior of
// the app (diffent loading times on each responce, handling exeptions)
///

void main() {
  runApp(
    ProviderScope(
      overrides: [
        // taskRepositoryProvider.overrideWithValue(FakeTasksRepository()),
        // userRepositoryProvider.overrideWithValue(FakeUsersRepository()),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      navigatorKey: AppRouter.navKey,
      theme: ThemeData(
        primaryColor: AppColors.primary,
        accentColor: AppColors.secondary,
        scaffoldBackgroundColor: AppColors.whitePrimary,
        appBarTheme: AppBarTheme(
          iconTheme: IconThemeData(color: AppColors.primary),
          color: AppColors.whitePrimary,
          elevation: 0,
        ),
      ),
      home: ProviderListener(
        onChange: (context, StateController<Exception?> exception) {
          Fluttertoast.showToast(
            msg: exception.state?.toString() ?? 'An error occured',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.SNACKBAR,
            timeInSecForIosWeb: 1,
            backgroundColor: AppColors.primary,
            textColor: Colors.white,
            fontSize: 16.0,
          );
        },
        provider: exceptionProvider,
        child: Home(),
      ),
      onGenerateRoute: AppRouter.generateRoute,
    );
  }
}

class Home extends StatelessWidget {
  static const name = '/';

  final GlobalKey<NavigatorState> _navKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Navigator(
        key: _navKey,
        initialRoute: TasksScreen.name,
        onGenerateRoute: (settings) {
          switch (settings.name) {
            case TasksScreen.name:
              return SlideRoute(
                page: TasksScreen(),
                name: TasksScreen.name,
                transitionDirection: TransitionDirection.RTL,
              );
            case UsersSreen.name:
              return SlideRoute(page: UsersSreen(), name: UsersSreen.name);
          }
        },
      ),
      bottomNavigationBar: AppButtomNavBar(
        initialIndex: 0,
        onSelection: (index) => index == 0
            ? _navKey.currentState
                ?.pushNamedReplacementIfNotCurrent(TasksScreen.name)
            : _navKey.currentState
                ?.pushNamedReplacementIfNotCurrent(UsersSreen.name),
      ),
    );
  }
}
