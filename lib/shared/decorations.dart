import 'package:flutter/material.dart';

import 'app_colors.dart';

BoxDecoration flatDecoration = BoxDecoration(
  color: AppColors.whitePrimary,
  borderRadius: BorderRadius.circular(16),
  boxShadow: [
    BoxShadow(
      color: Color.lerp(AppColors.whitePrimary, Colors.white, .9)!,
      offset: Offset(-3, -3),
      blurRadius: 6,
    ),
    BoxShadow(
      color: Color.lerp(AppColors.whitePrimary, Colors.black, 0.1)!,
      offset: Offset(6, 6),
      blurRadius: 6,
    )
  ],
);

BoxDecoration emposeDecoration = BoxDecoration(
  color: AppColors.whitePrimary,
  borderRadius: BorderRadius.circular(16),
  boxShadow: [
    BoxShadow(
      color: Color.lerp(AppColors.whitePrimary, Colors.black, 0.1)!,
      offset: Offset(-3, -3),
      blurRadius: 6,
    ),
    BoxShadow(
      color: Color.lerp(AppColors.whitePrimary, Colors.white, .9)!,
      offset: Offset(6, 6),
      blurRadius: 12,
    )
  ],
);

const BoxDecoration floatingActionDecoration = BoxDecoration(
  shape: BoxShape.circle,
  gradient: LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [
      AppColors.orangeSecondary,
      AppColors.orangePrimary,
      AppColors.primary
    ],
  ),
);

InputDecoration textInputDecoration = InputDecoration(
  contentPadding: EdgeInsets.only(left: 20),
  labelStyle: TextStyle(
    fontSize: 16,
    color: AppColors.secondary100,
  ),
  focusedBorder: OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.elliptical(10, 10),
    ),
    borderSide: BorderSide(
      color: AppColors.secondary200,
      width: 1,
    ),
  ),
  enabledBorder: OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.elliptical(10, 10),
    ),
    borderSide: BorderSide(
      color: AppColors.secondary100,
      width: 1,
    ),
  ),
  disabledBorder: OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.elliptical(10, 10),
    ),
    borderSide: BorderSide(
      color: AppColors.secondary300,
      width: 1,
    ),
  ),
  focusedErrorBorder: OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.elliptical(10, 10),
    ),
    borderSide: BorderSide(
      color: AppColors.primary100,
      width: 1,
    ),
  ),
  errorBorder: OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.elliptical(10, 10),
    ),
    borderSide: BorderSide(
      color: AppColors.primary100,
      width: 1,
    ),
  ),
  errorStyle: TextStyle(
    color: AppColors.primary100,
    fontSize: 10,
  ),
);
