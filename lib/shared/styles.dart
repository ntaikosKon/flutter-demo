import 'package:flutter/material.dart';

import 'app_colors.dart';

final btnStyle = ButtonStyle(
  padding: MaterialStateProperty.resolveWith(
    (states) => EdgeInsets.all(0),
  ),
  overlayColor: MaterialStateColor.resolveWith(
    (states) => AppColors.primary300.withOpacity(0.5),
  ),
);

final btnTextStyle = TextStyle(
  color: AppColors.primary,
  fontSize: 16,
  fontWeight: FontWeight.w600,
);

final labelTextStyle = TextStyle(
  color: AppColors.secondary,
  fontWeight: FontWeight.w600,
  fontSize: 14,
);

final contentTextStyle = TextStyle(
  color: AppColors.secondary,
  fontWeight: FontWeight.w400,
  fontSize: 12,
);

final titleTextStyle = TextStyle(
  color: AppColors.primary,
  fontSize: 28,
);

final chipTextStyle = TextStyle(
  color: AppColors.secondary,
  fontWeight: FontWeight.bold,
  fontSize: 12,
);
