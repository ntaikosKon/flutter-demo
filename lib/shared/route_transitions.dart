import 'package:flutter/material.dart';

enum TransitionDirection { BOTTOM_TO_TOP, LTR, RTL }

class SlideRoute extends PageRouteBuilder {
  final TransitionDirection transitionDirection;
  final Widget page;
  final String? name;

  SlideRoute({
    required this.page,
    this.name,
    this.transitionDirection = TransitionDirection.LTR,
  }) : super(
          transitionDuration: const Duration(milliseconds: 300),
          settings: RouteSettings(name: name),
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              SlideTransition(
            position: new Tween<Offset>(
              begin: transitionDirection == TransitionDirection.LTR
                  ? const Offset(1.0, 0.0)
                  : transitionDirection == TransitionDirection.RTL
                      ? const Offset(-1.0, 0.0)
                      : const Offset(0.0, 1.0),
              end: Offset.zero,
            ).animate(animation),
            child: page,
          ),
        );
}
