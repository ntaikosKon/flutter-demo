import 'package:flutter/material.dart';

class AppColors {
  static const Color whitePrimary = Color(0xFFF8F8F8);

  static const Color whiteSecondary = Color(0xFFFFFFFF);

  static const Color primary = Color(0xFFDF7B7B);
  static const Color primary100 = Color(0xFFEF6060);
  static const Color primary200 = Color(0xFFFF8282);
  static const Color primary300 = Color(0xFFF0D2D2);

  static const Color secondary = Color(0xFF485665);
  static const Color secondary100 = Color(0xFF647587);
  static const Color secondary200 = Color(0xFF8799AC);
  static const Color secondary300 = Color(0xFFA8BACC);
  static const Color secondary400 = Color(0xFFCDD4DB);

  static const Color orangePrimary = Color(0xFFEFA69B);

  static const Color orangeSecondary = Color(0xFFFFD0B9);
}
