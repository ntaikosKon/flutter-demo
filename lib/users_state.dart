import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'models/models.dart';
import 'providers/providers.dart';
import 'repositories/repositories.dart';

final usersStateProvider = StateNotifierProvider<UsersState>((ref) {
  return UsersState(ref.read);
});

final userByIdProvider = Provider.family<AsyncValue<User>, String>((ref, id) {
  final users = ref.watch(usersStateProvider.state);

  return users.whenData((users) => users.firstWhere((user) => user.id == id));
});

class UsersState extends StateNotifier<AsyncValue<List<User>>> {
  final Reader read;

  UsersState(
    this.read, [
    AsyncValue<List<User>>? users,
  ]) : super(users ?? AsyncValue.loading()) {
    _getUsers();
  }

  void _getUsers() async {
    try {
      final tasks = await read(userRepositoryProvider).getUsers();
      state = AsyncValue.data(tasks);
    } catch (e) {
      state = AsyncValue.error(e);
    }
  }

  Future<void> addUser({
    required String name,
    required UserRole role,
  }) async {
    try {
      final newUser =
          await read(userRepositoryProvider).addUser(name: name, role: role);
      state = state.whenData((users) => [...users]..add(newUser));
    } on UserException catch (e) {
      _handleException(e);
    }
  }

  Future<void> updateUser({
    required String id,
    String? name,
    UserRole? role,
  }) async {
    try {
      final updatedUser = await read(userRepositoryProvider).updateUser(
        id: id,
        name: name,
        role: role,
      );

      state = state.whenData(
        (users) => users.map(
          (user) {
            if (user.id == id) {
              return updatedUser;
            }
            return user;
          },
        ).toList(),
      );
    } on UserException catch (e) {
      _handleException(e);
    }
  }

  void _handleException(UserException e) {
    read(exceptionProvider).state = e;
  }
}
