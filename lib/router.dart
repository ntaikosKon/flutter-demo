import 'package:demo/main.dart';
import 'package:flutter/material.dart';

import 'screens/screens.dart';
import 'shared/route_transitions.dart';

class AppRouter {
  static final GlobalKey<NavigatorState> navKey = GlobalKey<NavigatorState>();

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Home.name:
        return MaterialPageRoute(builder: (_) => Home());
      case ManageTaskScreen.name:
        dynamic? args = settings.arguments;
        return SlideRoute(
          page: ManageTaskScreen(
            taskId: args?['taskId'],
          ),
          name: ManageTaskScreen.name,
          transitionDirection: TransitionDirection.BOTTOM_TO_TOP,
        );
      case ManageUserScreen.name:
        dynamic? args = settings.arguments;
        return SlideRoute(
          page: ManageUserScreen(
            userId: args?['userId'],
          ),
          name: ManageUserScreen.name,
          transitionDirection: TransitionDirection.BOTTOM_TO_TOP,
        );
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(child: Text('No route defined for ${settings.name}')),
          ),
        );
    }
  }

  static pushNamed(String name, {Object? arguments}) {
    AppRouter.navKey.currentState?.pushNamed(
      name,
      arguments: arguments,
    );
  }

  static pop() {
    AppRouter.navKey.currentState?.pop();
  }
}
