import 'package:demo/models/models.dart';
import 'package:demo/shared/app_colors.dart';
import 'package:demo/shared/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../widgets/widgets.dart';
import '../shared/decorations.dart';
import '../shared/validators.dart';
import '../router.dart';
import '../users_state.dart';

class ManageUserScreen extends HookWidget {
  static const name = '/manage_user';
  final String? userId;
  final _key = GlobalKey<FormState>();

  ManageUserScreen({Key? key, this.userId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = userId == null ? null : useProvider(userByIdProvider(userId!));

    final _nameController = useTextEditingController(text: null);
    final _isLoading = useState(false);
    final _selectedRole = useState<UserRole?>(null);

    useEffect(() {
      user?.whenData((value) {
        _nameController.text = value.name;
        _selectedRole.value = value.role;
      });
    }, [user]);

    final _onSavePressed = () async {
      FocusScope.of(context).unfocus();
      final result = _key.currentState?.validate();
      if (result == null || !result || _selectedRole.value == null) return;
      _isLoading.value = true;

      if (userId == null) {
        await context
            .read(usersStateProvider)
            .addUser(name: _nameController.text, role: _selectedRole.value!);
      } else {
        await context.read(usersStateProvider).updateUser(
              id: userId!,
              name: _nameController.text,
              role: _selectedRole.value,
            );
      }
      AppRouter.pop();
    };

    return ScreenLayout(
      title: userId == null ? 'New User' : 'Edit User',
      body: SingleChildScrollView(
        child: FormLayout(
          formkey: _key,
          inputFields: [
            TextFormField(
              decoration: textInputDecoration.copyWith(labelText: 'Name'),
              controller: _nameController,
              validator: requiredValidation,
            ),
            _RoleSelection(
              selecterRole: _selectedRole.value,
              onTap: (role) => _selectedRole.value = role,
            )
          ],
          saveBtn: AppButton(
            width: MediaQuery.of(context).size.width * 0.5,
            height: 50,
            onTap: _onSavePressed,
            text: 'SAVE',
            enabled: !_isLoading.value,
          ),
        ),
      ),
    );
  }
}

class _RoleSelection extends StatelessWidget {
  final UserRole? selecterRole;
  final void Function(UserRole) onTap;

  _RoleSelection({
    Key? key,
    required this.selecterRole,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _manageSelection = () async {
      final result = await showMaterialModalBottomSheet<UserRole?>(
        context: context,
        builder: (context) => const _RoleSelector(),
        useRootNavigator: true,
      );

      if (result != null) {
        onTap(result);
      }
    };

    return GestureDetector(
      onTap: _manageSelection,
      child: Container(
        width: double.infinity,
        height: 50,
        decoration: BoxDecoration(
          border: Border.all(color: AppColors.secondary),
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Text(
              selecterRole == null
                  ? 'Select Role'
                  : selecterRole == UserRole.DESIGNER
                      ? 'Designer'
                      : 'Developer',
              style: contentTextStyle,
            ),
          ),
        ),
      ),
    );
  }
}

class _RoleSelector extends StatelessWidget {
  const _RoleSelector({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: SafeArea(
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ListTile(
                title: UserRoleChip(
                  role: UserRole.DESIGNER,
                ),
                onTap: () => Navigator.of(context).pop(UserRole.DESIGNER),
              ),
              ListTile(
                title: UserRoleChip(
                  role: UserRole.DEVELOPER,
                ),
                onTap: () => Navigator.of(context).pop(UserRole.DEVELOPER),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
