import 'package:demo/models/models.dart';
import 'package:demo/shared/app_colors.dart';
import 'package:demo/shared/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../task_state.dart';
import '../widgets/widgets.dart';
import '../shared/decorations.dart';
import '../screens/manage_task.dart';
import '../router.dart';

enum TaskStatusFilter {
  ALL,
  IN_PROGRESS,
  TO_DO,
}
final _taskStatusFilter =
    StateProvider.autoDispose((_) => TaskStatusFilter.ALL);

final _filteredTasks = Provider.autoDispose<AsyncValue<List<Task>>>((ref) {
  final filter = ref.watch(_taskStatusFilter);
  final tasks = ref.watch(tasksStateProvider.state);

  switch (filter.state) {
    case TaskStatusFilter.IN_PROGRESS:
      return tasks.whenData(
        ((tasks) => tasks
            .where((task) => task.status == TaskStatus.IN_PROGRESS)
            .toList()),
      );
    case TaskStatusFilter.TO_DO:
      return tasks.whenData(
        ((tasks) =>
            tasks.where((task) => task.status == TaskStatus.TO_DO).toList()),
      );
    case TaskStatusFilter.ALL:
    default:
      return tasks;
  }
});

class TasksScreen extends HookWidget {
  static const name = '/tasks';

  @override
  Widget build(BuildContext context) {
    final tasks = useProvider(_filteredTasks);
    final filter = useProvider(_taskStatusFilter);

    return ScreenLayout(
      title: 'Tasks',
      menu: CustomMenu(
        actions: [
          CustomMenuAction(
            key: Key('menu-status-all'),
            title: 'ALL',
            callback: () => filter.state = TaskStatusFilter.ALL,
          ),
          CustomMenuAction(
            key: Key('menu-status-inProgress'),
            title: 'IN PROGRESS',
            callback: () => filter.state = TaskStatusFilter.IN_PROGRESS,
          ),
          CustomMenuAction(
            key: Key('menu-status-toDo'),
            title: 'TO DO',
            callback: () => filter.state = TaskStatusFilter.TO_DO,
          )
        ],
        selectedAction: filter.state.index,
      ),
      body: tasks.when(
        data: (tasks) => TasksList(tasks: tasks),
        loading: () => Center(
          child: CircularProgressIndicator(),
        ),
        error: (__, _) => Center(
          child: Text(
            'Error while loading tasks',
            style: labelTextStyle.copyWith(color: AppColors.primary300),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.transparent,
        child: Container(
          height: 60,
          width: 60,
          decoration: floatingActionDecoration,
          child: Icon(Icons.add),
        ),
        onPressed: () async {
          AppRouter.pushNamed(ManageTaskScreen.name);
        },
      ),
    );
  }
}
