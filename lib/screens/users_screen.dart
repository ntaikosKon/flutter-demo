import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../router.dart';
import '../widgets/widgets.dart';
import '../shared/styles.dart';
import '../shared/app_colors.dart';
import '../users_state.dart';
import '../screens/screens.dart';
import '../shared/decorations.dart';
import '../models/models.dart';

enum UserRoleFilter {
  ALL,
  DEVELOPER,
  DESIGNER,
}

final userRoleFilter = StateProvider.autoDispose((_) => UserRoleFilter.ALL);

final filteredUsers = Provider.autoDispose<AsyncValue<List<User>>>((ref) {
  final filter = ref.watch(userRoleFilter);
  final users = ref.watch(usersStateProvider.state);

  switch (filter.state) {
    case UserRoleFilter.DESIGNER:
      return users.whenData(
        ((users) =>
            users.where((user) => user.role == UserRole.DESIGNER).toList()),
      );
    case UserRoleFilter.DEVELOPER:
      return users.whenData(
        ((users) =>
            users.where((user) => user.role == UserRole.DEVELOPER).toList()),
      );
    case UserRoleFilter.ALL:
    default:
      return users;
  }
});

class UsersSreen extends HookWidget {
  static const name = '/users';

  @override
  Widget build(BuildContext context) {
    final users = useProvider(filteredUsers);
    final filter = useProvider(userRoleFilter);

    return ScreenLayout(
      title: 'Users',
      menu: CustomMenu(
        actions: [
          CustomMenuAction(
            title: 'ALL',
            callback: () => filter.state = UserRoleFilter.ALL,
          ),
          CustomMenuAction(
            title: 'DEVELOPERS',
            callback: () => filter.state = UserRoleFilter.DEVELOPER,
          ),
          CustomMenuAction(
            title: 'DESIGNERS',
            callback: () => filter.state = UserRoleFilter.DESIGNER,
          )
        ],
        selectedAction: filter.state.index,
      ),
      body: users.when(
        data: (users) => UsersList(
          users: users,
        ),
        loading: () => Center(
          child: CircularProgressIndicator(),
        ),
        error: (__, _) => Center(
            child: Text(
          'Error',
          style: labelTextStyle.copyWith(
            color: AppColors.secondary,
          ),
        )),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.transparent,
        child: Container(
          height: 60,
          width: 60,
          decoration: floatingActionDecoration,
          child: Icon(Icons.add),
        ),
        onPressed: () async {
          AppRouter.pushNamed(ManageUserScreen.name);
        },
      ),
    );
  }
}
