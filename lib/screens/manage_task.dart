import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../widgets/widgets.dart';
import '../shared/decorations.dart';
import '../shared/validators.dart';
import '../router.dart';
import '../task_state.dart';

class ManageTaskScreen extends HookWidget {
  static const name = '/manage_task';
  final String? taskId;
  final _key = GlobalKey<FormState>();

  ManageTaskScreen({Key? key, this.taskId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final task = taskId == null ? null : useProvider(taskByIdProvider(taskId!));

    final _nameController = useTextEditingController(text: null);
    final _descriptionController = useTextEditingController(text: null);
    final _isLoading = useState(false);

    useEffect(() {
      task?.whenData((value) {
        _nameController.text = value.title;
        _descriptionController.text = value.description;
      });
    }, [task]);

    final _onSavePressed = () async {
      FocusScope.of(context).unfocus();
      final result = _key.currentState?.validate();
      if (result != null && !result) return;
      _isLoading.value = true;

      if (taskId == null) {
        await context.read(tasksStateProvider).addTask(
              title: _nameController.text,
              description: _descriptionController.text,
            );
      } else {
        await context.read(tasksStateProvider).updateTask(
              id: taskId!,
              title: _nameController.text,
              description: _descriptionController.text,
            );
      }
      AppRouter.pop();
    };

    return ScreenLayout(
      title: taskId == null ? 'New Task' : 'Edit Task',
      body: SingleChildScrollView(
        child: FormLayout(
          formkey: _key,
          inputFields: [
            TextFormField(
              decoration: textInputDecoration.copyWith(labelText: 'Title'),
              textInputAction: TextInputAction.next,
              controller: _nameController,
              validator: requiredValidation,
            ),
            TextFormField(
              decoration: textInputDecoration.copyWith(
                labelText: 'Description',
                isDense: true,
                alignLabelWithHint: true,
                contentPadding: const EdgeInsets.only(left: 20, top: 30),
              ),
              maxLines: 8,
              maxLength: 200,
              textInputAction: TextInputAction.newline,
              validator: requiredValidation,
              controller: _descriptionController,
            )
          ],
          saveBtn: AppButton(
            width: MediaQuery.of(context).size.width * 0.5,
            height: 50,
            onTap: _onSavePressed,
            text: 'SAVE',
            enabled: !_isLoading.value,
          ),
        ),
      ),
    );
  }
}
