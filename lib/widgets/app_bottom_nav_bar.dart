import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import '../shared/app_colors.dart';
import '../shared/decorations.dart';

class AppButtomNavBar extends HookWidget {
  final void Function(int) onSelection;
  final int initialIndex;

  AppButtomNavBar({
    required this.onSelection,
    required this.initialIndex,
  });

  @override
  Widget build(BuildContext context) {
    final selectedIndex = useState(0);

    return Container(
      margin: const EdgeInsets.all(16),
      padding: const EdgeInsets.all(8),
      decoration: emposeDecoration,
      child: BottomNavigationBar(
        elevation: 0,
        backgroundColor: AppColors.whitePrimary,
        currentIndex: selectedIndex.value,
        onTap: (index) {
          selectedIndex.value = index;
          onSelection(index);
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.assignment_outlined),
            label: 'Tasks',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.people_alt),
            label: 'Users',
          ),
        ],
      ),
    );
  }
}
