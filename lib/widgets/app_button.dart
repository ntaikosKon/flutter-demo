import 'dart:ui';
import 'dart:math';

import 'package:flutter/material.dart';

import '../shared/app_colors.dart';

class AppButton extends StatelessWidget {
  final void Function() onTap;
  final String text;
  final bool enabled;
  final bool isLoading;
  final double height;
  final double width;

  const AppButton({
    required this.onTap,
    required this.text,
    required this.enabled,
    required this.height,
    required this.width,
    this.isLoading = false,
    Key? key,
  }) : super(key: key);

  double _getWidth() {
    if (enabled && !isLoading) {
      return width;
    } else if (enabled && isLoading) {
      return height;
    } else {
      return 0.0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: enabled && !isLoading ? onTap : null,
      child: Container(
        height: height,
        width: width,
        child: Stack(
          children: [
            // The gray backround when the button is disabled
            Container(
              height: height,
              child: Center(
                child: AnimatedContainer(
                  duration: Duration(milliseconds: 250),
                  height: height,
                  width: !isLoading ? width : height,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: AppColors.secondary400,
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.16),
                        blurRadius: 6,
                        offset: Offset(0, 3),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            //The blue backround when the button is enabled
            Container(
              height: height,
              child: Center(
                child: AnimatedContainer(
                  duration: Duration(milliseconds: 250),
                  height: enabled ? height : 0,
                  width: _getWidth(),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: AppColors.secondary,
                  ),
                ),
              ),
            ),
            // The text should be placed last to be on top. We use container with center
            Container(
              height: height,
              child: Center(
                child: AnimatedCrossFade(
                  duration: Duration(milliseconds: 250),
                  crossFadeState: !isLoading
                      ? CrossFadeState.showFirst
                      : CrossFadeState.showSecond,
                  firstChild: Text(
                    text,
                    style: TextStyle(
                      color: enabled
                          ? AppColors.whiteSecondary
                          : AppColors.secondary200,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  secondChild: _LoadingIndicator(
                    size: height,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _LoadingIndicator extends StatefulWidget {
  final double _size;

  _LoadingIndicator({required double size}) : _size = size;

  @override
  _LoadingIndicatorState createState() => _LoadingIndicatorState();
}

class _LoadingIndicatorState extends State<_LoadingIndicator>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    );

    _animation = Tween<double>(begin: 0, end: 2 * pi).animate(controller);

    controller.repeat();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget._size,
      width: widget._size,
      child: CustomPaint(
        painter: WaveingDotPainter(_animation),
      ),
    );
  }
}

class WaveingDotPainter extends CustomPainter {
  Animation<double> _animation;
  final Paint _paint;

  WaveingDotPainter(this._animation)
      : _paint = Paint()
          ..color = Colors.white
          ..strokeCap = StrokeCap.round
          ..strokeWidth = 4,
        super(repaint: _animation);

  @override
  void paint(Canvas canvas, Size size) {
    double value = _animation.value;
    double baseHeight = size.height / 2;

    double firstDotHeight = baseHeight - (size.height / 16) * cos(value);
    double secondDotHeight =
        baseHeight - (size.height / 16) * cos(value + pi / 2);
    double thirdDotHeight = baseHeight - (size.height / 16) * cos(value + pi);

    List<Offset> points = [
      Offset(size.width / 2 - 10, firstDotHeight),
      Offset(size.width / 2, secondDotHeight),
      Offset(size.width / 2 + 10, thirdDotHeight),
    ];

    canvas.drawPoints(PointMode.points, points, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
