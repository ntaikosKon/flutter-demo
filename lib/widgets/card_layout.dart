import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import '../shared/decorations.dart';

class CardLayout extends HookWidget {
  final Widget topSection;
  final Widget expandedSection;

  CardLayout({required this.topSection, required this.expandedSection});

  @override
  Widget build(BuildContext context) {
    final _controller = useAnimationController(duration: Duration(seconds: 1));
    final _focusNode = useFocusNode();

    final _sizeAnimation = Tween<double>(begin: 0, end: 1).animate(
      CurvedAnimation(parent: _controller, curve: Interval(0, 0.4)),
    );
    final _opacityAnimation = Tween<double>(begin: 0, end: 1).animate(
      CurvedAnimation(parent: _controller, curve: Interval(0.6, 1)),
    );

    return Focus(
      focusNode: _focusNode,
      onFocusChange: (hasFocus) =>
          hasFocus ? _controller.forward() : _controller.reverse(),
      child: GestureDetector(
        onTap: () => _focusNode.hasFocus
            ? _focusNode.unfocus()
            : _focusNode.requestFocus(),
        child: Container(
          padding: const EdgeInsets.all(16.0),
          margin: const EdgeInsets.all(16.0),
          width: MediaQuery.of(context).size.width * 0.7,
          decoration: flatDecoration,
          child: Column(
            children: [
              topSection,
              AnimatedBuilder(
                animation: _controller,
                builder: (_, __) => SizeTransition(
                  sizeFactor: _sizeAnimation,
                  child: Opacity(
                    opacity: _opacityAnimation.value,
                    child: expandedSection,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
