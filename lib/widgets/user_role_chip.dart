import 'package:demo/shared/styles.dart';
import 'package:flutter/material.dart';

import '../models/models.dart';
import '../shared/app_colors.dart';

class UserRoleChip extends StatelessWidget {
  final UserRole role;

  const UserRoleChip({Key? key, required this.role}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (role) {
      case UserRole.DESIGNER:
        return Chip(
          label: Text('DESIGNER'),
          backgroundColor: AppColors.primary300,
          labelStyle: chipTextStyle,
        );
      case UserRole.DEVELOPER:
        return Chip(
          label: Text(
            'DEVELOPER',
            style: TextStyle(color: AppColors.whitePrimary),
          ),
          backgroundColor: AppColors.secondary,
          labelStyle: chipTextStyle,
        );
    }
  }
}
