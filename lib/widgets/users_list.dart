import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../models/models.dart';
import '../router.dart';
import '../shared/styles.dart';
import '../task_state.dart';
import '../widgets/widgets.dart';
import '../screens/screens.dart';

class UsersList extends StatelessWidget {
  final List<User> users;

  const UsersList({Key? key, required this.users}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ...users
            .map(
              (user) => ProviderScope(
                overrides: [_currentUser.overrideWithValue(user)],
                child: const _UserItem(),
              ),
            )
            .toList()
      ],
    );
  }
}

final _currentUser = ScopedProvider<User>(null);

class _UserItem extends HookWidget {
  const _UserItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = useProvider(_currentUser);

    return CardLayout(
      topSection: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Name',
                style: labelTextStyle,
              ),
              SizedBox(height: 5),
              Text(
                user.name,
                style: contentTextStyle,
              ),
            ],
          ),
          UserRoleChip(role: user.role),
        ],
      ),
      expandedSection: _ExpandedSection(userId: user.id),
    );
  }
}

class _ExpandedSection extends StatelessWidget {
  final String userId;

  const _ExpandedSection({Key? key, required this.userId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Divider(
            thickness: 2,
          ),
          Text(
            'Assigned Tasks',
            style: labelTextStyle,
          ),
          Consumer(builder: (context, watch, child) {
            final tasks = watch(tasksByUserProvider(userId));
            return tasks.when(
              data: (tasks) => Container(
                child: Column(
                  children: [
                    if (tasks.length > 0)
                      ...tasks
                          .map(
                            (task) => Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(task.title),
                                TaskStatusChip(status: task.status)
                              ],
                            ),
                          )
                          .toList()
                    else
                      CenteredContainer(
                        child: Text(
                          'The user has no tasks assigned',
                          style: contentTextStyle,
                        ),
                      ),
                  ],
                ),
              ),
              loading: () => CenteredContainer(
                child: Text(
                  'Loading Tasks...',
                  style: contentTextStyle,
                ),
              ),
              error: (_, __) => CenteredContainer(
                child: Text(
                  'Error while feching tasks',
                  style: contentTextStyle,
                ),
              ),
            );
          }),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextButton(
                onPressed: () => AppRouter.pushNamed(
                  ManageUserScreen.name,
                  arguments: {'userId': userId},
                ),
                child: Text(
                  'EDIT',
                  style: btnTextStyle,
                ),
                style: btnStyle,
              ),
            ],
          )
        ],
      ),
    );
  }
}
