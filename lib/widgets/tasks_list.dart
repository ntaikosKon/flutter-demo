import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../models/models.dart';
import '../shared/styles.dart';
import '../users_state.dart';
import 'widgets.dart';
import '../router.dart';
import '../screens/screens.dart';
import '../task_state.dart';

class TasksList extends StatelessWidget {
  final List<Task> tasks;

  const TasksList({Key? key, required this.tasks}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ...tasks
            .map(
              (task) => ProviderScope(
                overrides: [_currentTask.overrideWithValue(task)],
                child: const _TaskItem(),
              ),
            )
            .toList()
      ],
    );
  }
}

final _currentTask = ScopedProvider<Task>(null);

class _TaskItem extends HookWidget {
  const _TaskItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final task = useProvider(_currentTask);

    return CardLayout(
      topSection: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Title',
                style: labelTextStyle,
              ),
              SizedBox(height: 5),
              Text(
                task.title,
                style: contentTextStyle,
              ),
            ],
          ),
          TaskStatusChip(status: task.status)
        ],
      ),
      expandedSection: const _ExpandedSection(),
    );
  }
}

class _ExpandedSection extends HookWidget {
  const _ExpandedSection();

  @override
  Widget build(BuildContext context) {
    final task = useProvider(_currentTask);

    final _changeTaskStatus = () async {
      final result = await showMaterialModalBottomSheet<TaskStatus>(
        context: context,
        builder: (context) => const _StatusSelector(),
        useRootNavigator: true,
      );

      if (result != null) {
        await context
            .read(tasksStateProvider)
            .updateTask(id: task.id, status: result);
      }
    };

    final _changeUser = () async {
      final result = await showMaterialModalBottomSheet<String>(
        context: context,
        builder: (context) => const _UserSelector(),
        useRootNavigator: true,
      );

      if (result != null) {
        await context
            .read(tasksStateProvider)
            .updateTask(id: task.id, userId: result);
      }
    };

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Divider(thickness: 2),
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Description', style: labelTextStyle),
                SizedBox(height: 5),
                Text(
                  task.description,
                  style: contentTextStyle,
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Asigned to', style: labelTextStyle),
                SizedBox(height: 5),
                Consumer(
                  builder: (context, watch, child) {
                    final user = task.userId != null
                        ? watch(userByIdProvider(task.userId!))
                        : null;

                    return user?.when(
                          data: (user) => Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(user.name, style: contentTextStyle),
                              UserRoleChip(role: user.role)
                            ],
                          ),
                          loading: () => Text(
                            'Loading ...',
                            style: contentTextStyle,
                          ),
                          error: (_, __) => Text(
                            'Error while loading ...',
                            style: contentTextStyle,
                          ),
                        ) ??
                        Text(
                          'Not Assigned To User',
                          style: contentTextStyle,
                        );
                  },
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextButton(
                onPressed: () => AppRouter.pushNamed(
                  ManageTaskScreen.name,
                  arguments: {'taskId': task.id},
                ),
                child: Text(
                  'EDIT',
                  style: btnTextStyle,
                ),
                style: btnStyle,
              ),
              TextButton(
                onPressed: _changeTaskStatus,
                child: Text(
                  'STATUS',
                  style: btnTextStyle,
                ),
                style: btnStyle,
              ),
              TextButton(
                onPressed: _changeUser,
                child: Text(
                  task.userId == null ? 'ADD USER' : 'CHANGE USER',
                  style: btnTextStyle,
                ),
                style: btnStyle,
              )
            ],
          )
        ],
      ),
    );
  }
}

class _StatusSelector extends StatelessWidget {
  const _StatusSelector({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: SafeArea(
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ListTile(
                title: TaskStatusChip(
                  status: TaskStatus.TO_DO,
                ),
                onTap: () => Navigator.of(context).pop(TaskStatus.TO_DO),
              ),
              ListTile(
                title: TaskStatusChip(
                  status: TaskStatus.IN_PROGRESS,
                ),
                onTap: () => Navigator.of(context).pop(TaskStatus.IN_PROGRESS),
              ),
              ListTile(
                title: TaskStatusChip(
                  status: TaskStatus.DONE,
                ),
                onTap: () => Navigator.of(context).pop(TaskStatus.DONE),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _UserSelector extends HookWidget {
  const _UserSelector({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final users = useProvider(usersStateProvider.state);

    return Material(
      child: SafeArea(
        child: users.when(
          data: (users) => Container(
            constraints: BoxConstraints(
              maxHeight: MediaQuery.of(context).size.height * 0.4,
            ),
            child: ListView.builder(
              itemCount: users.length,
              itemBuilder: (_, i) {
                final user = users[i];
                return ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(user.name),
                      UserRoleChip(role: user.role),
                    ],
                  ),
                  onTap: () => Navigator.of(context).pop(user.id),
                );
              },
            ),
          ),
          loading: () => CenteredContainer(
            child: Text(
              'Loading users ...',
              style: labelTextStyle,
            ),
          ),
          error: (_, __) => CenteredContainer(
            child: Text(
              'Loading users ...',
              style: labelTextStyle,
            ),
          ),
        ),
      ),
    );
  }
}
