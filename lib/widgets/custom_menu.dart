import 'package:flutter/material.dart';

import '../shared/app_colors.dart';
import '../shared/clip_shadow.dart';

class CustomMenuAction {
  final String title;
  final VoidCallback callback;
  final Key? key;

  CustomMenuAction({
    this.key,
    required this.title,
    required this.callback,
  });
}

class CustomMenu extends StatelessWidget {
  final List<CustomMenuAction> _actions;
  final int _seletedAction;

  CustomMenu({
    required List<CustomMenuAction> actions,
    required int selectedAction,
  })   : _actions = actions,
        _seletedAction = selectedAction;

  @override
  Widget build(BuildContext context) {
    final TextStyle _style = TextStyle(
      color: AppColors.secondary100,
      fontSize: 14,
    );

    const EdgeInsetsGeometry _btnPadding = EdgeInsets.symmetric(vertical: 8.0);

    return ClipShadow(
      clipper: _Clipper(),
      boxShadow: [
        BoxShadow(
          color: Color.fromRGBO(0, 0, 0, 0.16),
          blurRadius: 6,
          spreadRadius: 2,
          offset: Offset(0, 3),
        )
      ],
      child: Container(
        color: AppColors.whitePrimary,
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.topRight,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              ..._actions
                  .asMap()
                  .entries
                  .map(
                    (entry) => InkWell(
                      key: entry.value.key,
                      onTap: () {
                        FocusScope.of(context).unfocus();
                        entry.value.callback();
                      },
                      child: Container(
                        padding: _btnPadding,
                        child: Text(
                          entry.value.title,
                          style: entry.key == _seletedAction
                              ? _style.copyWith(fontWeight: FontWeight.bold)
                              : _style,
                        ),
                      ),
                    ),
                  )
                  .toList()
            ],
          ),
        ),
      ),
    );
  }
}

class _Clipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path()..moveTo(0, 0);
    path.lineTo(0, 5);
    path.lineTo(size.width / 2, 5);
    var controlPoint = Offset((2 * size.width) / 3 - 30, size.height);
    var endPoint = Offset((0.85 * size.width), size.height);

    path.quadraticBezierTo(
        controlPoint.dx, controlPoint.dy, endPoint.dx, endPoint.dy);

    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);

    return path;
  }

  @override
  bool shouldReclip(oldCliper) => false;
}
