import 'package:flutter/material.dart';

import 'package:demo/shared/decorations.dart';

class FormLayout extends StatelessWidget {
  final GlobalKey<FormState> formkey;
  final List<Widget> inputFields;
  final Widget saveBtn;

  const FormLayout({
    Key? key,
    required this.formkey,
    required this.inputFields,
    required this.saveBtn,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.all(16),
            padding: const EdgeInsets.all(16),
            decoration: flatDecoration,
            child: Form(
              key: formkey,
              child: Column(
                children: [
                  ...inputFields
                      .map(
                        (field) => Padding(
                          padding: const EdgeInsets.only(bottom: 30),
                          child: field,
                        ),
                      )
                      .toList(),
                ],
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.all(16.0),
            padding: const EdgeInsets.all(32.0),
            decoration: flatDecoration,
            child: Center(child: saveBtn),
          ),
        ],
      ),
    );
  }
}
