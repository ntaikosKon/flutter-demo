import 'package:demo/widgets/custom_menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import '../shared/app_colors.dart';
import '../shared/styles.dart';
import 'animated_filter_icon.dart';

class ScreenLayout extends HookWidget {
  final String title;
  final Widget body;

  final Widget? floatingActionButton;
  final CustomMenu? menu;

  ScreenLayout({
    required this.title,
    required this.body,
    this.menu,
    this.floatingActionButton,
  });

  @override
  Widget build(BuildContext context) {
    final isMenuActive = useState(false);
    final _focusNode = useFocusNode();

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            title,
            style: titleTextStyle,
          ),
          actions: this.menu == null
              ? null
              : [
                  Focus(
                    focusNode: _focusNode,
                    onFocusChange: (hasFocus) => isMenuActive.value = hasFocus,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 16.0),
                      child: GestureDetector(
                        key: Key('menu-button'),
                        onTap: () => _focusNode.hasFocus
                            ? _focusNode.unfocus()
                            : _focusNode.requestFocus(),
                        child: AnimatedFilterIcon(
                          color: AppColors.primary,
                          duration: Duration(milliseconds: 500),
                          isOpen: isMenuActive.value,
                        ),
                      ),
                    ),
                  )
                ],
        ),
        floatingActionButton: floatingActionButton,
        body: Stack(
          children: [
            body,
            if (menu != null)
              AnimatedPositioned(
                top: 0,
                left:
                    isMenuActive.value ? 0 : MediaQuery.of(context).size.width,
                child: menu!,
                duration: const Duration(milliseconds: 500),
              ),
          ],
        ),
      ),
    );
  }
}
