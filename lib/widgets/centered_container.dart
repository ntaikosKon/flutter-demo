import 'package:flutter/material.dart';

class CenteredContainer extends StatelessWidget {
  final Widget child;

  const CenteredContainer({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: Center(
        child: child,
      ),
    );
  }
}
