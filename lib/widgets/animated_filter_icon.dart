import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class AnimatedFilterIcon extends HookWidget {
  final Duration _duration;
  final Color _color;
  final bool _isOpen;

  const AnimatedFilterIcon({
    required Duration duration,
    required Color color,
    required bool isOpen,
    Key? key,
  })  : _duration = duration,
        _color = color,
        _isOpen = isOpen,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    AnimationController _controller =
        useAnimationController(duration: _duration);

    useValueChanged<bool, void>(_isOpen, (isOpen, __) {
      isOpen ? _controller.reverse() : _controller.forward();
    });

    return Center(
      child: AnimatedBuilder(
        animation: _controller,
        builder: (_, __) => CustomPaint(
          painter: _IconPainter(
            controller: _controller,
            color: _color,
          ),
          child: SizedBox(
            height: 25,
            width: 25,
          ),
        ),
      ),
    );
  }
}

class _IconPainter extends CustomPainter {
  Paint _paint;
  AnimationController controller;
  Color _color;
  Animation<double> _animation;
  late Animation<Offset> _topLeftPoint;
  late Animation<Offset> _bottomLeftPoint;
  late Animation<Offset> _topRightPoint;
  late Animation<Offset> _bottomRightPoint;

  _IconPainter({required this.controller, required Color color})
      : _paint = Paint()
          ..color = color
          ..strokeWidth = 2.5
          ..strokeCap = StrokeCap.round,
        _animation = Tween<double>(begin: 0.0, end: 1.0).animate(controller),
        _color = color,
        super(repaint: controller);

  @override
  void paint(Canvas canvas, Size size) {
    double height = size.height;
    double width = size.width;

    _topLeftPoint = Tween<Offset>(
      begin: Offset(0.2 * width, 0.1 * height),
      end: Offset(0.1 * width, 0.1 * height),
    ).animate(controller);

    _bottomLeftPoint = Tween<Offset>(
      begin: Offset(0.2 * width, 0.9 * height),
      end: Offset(0.9 * width, 0.9 * height),
    ).animate(controller);

    _topRightPoint = Tween<Offset>(
      begin: Offset(0.8 * width, 0.1 * height),
      end: Offset(0.9 * width, 0.1 * height),
    ).animate(controller);

    _bottomRightPoint = Tween<Offset>(
      begin: Offset(0.8 * width, 0.9 * height),
      end: Offset(0.1 * width, 0.9 * height),
    ).animate(controller);

    // The first line
    canvas.drawLine(
      _topLeftPoint.value,
      _bottomLeftPoint.value,
      _paint,
    );

    // The second line
    _paint.color = _color.withOpacity(1 - _animation.value);

    canvas.drawLine(
      Offset(0.5 * width, 0.1 * height),
      Offset(0.5 * width, 0.9 * height),
      _paint,
    );

    _paint.color = _color;

    // The third line

    canvas.drawLine(
      _topRightPoint.value,
      _bottomRightPoint.value,
      _paint,
    );

    // The first circle
    _paint.color = _color.withOpacity(1 - _animation.value);
    canvas.drawCircle(
      Offset(0.2 * width, 0.3 * height),
      3,
      _paint,
    );

    _paint.color = Colors.white.withOpacity(1 - _animation.value);

    canvas.drawCircle(
      Offset(0.2 * width, 0.3 * height),
      1,
      _paint,
    );

    //The second circle

    _paint.color = _color.withOpacity(1 - _animation.value);
    canvas.drawCircle(
      Offset(0.5 * width, 0.7 * height),
      3,
      _paint,
    );

    _paint.color = Colors.white.withOpacity(1 - _animation.value);

    canvas.drawCircle(
      Offset(0.5 * width, 0.7 * height),
      1,
      _paint,
    );

    //The third circle
    _paint.color = _color.withOpacity(1 - _animation.value);
    canvas.drawCircle(
      Offset(0.8 * width, 0.3 * height),
      3,
      _paint,
    );

    _paint.color = Colors.white.withOpacity(1 - _animation.value);

    canvas.drawCircle(
      Offset(0.8 * width, 0.3 * height),
      1,
      _paint,
    );

    _paint.color = _color;
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
