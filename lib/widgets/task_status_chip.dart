import 'package:demo/shared/styles.dart';
import 'package:flutter/material.dart';

import '../models/models.dart';
import '../shared/app_colors.dart';

class TaskStatusChip extends StatelessWidget {
  final TaskStatus status;

  const TaskStatusChip({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (status) {
      case TaskStatus.TO_DO:
        return Chip(
          labelStyle: chipTextStyle,
          label: Text('TO DO'),
          backgroundColor: AppColors.primary300,
        );
      case TaskStatus.IN_PROGRESS:
        return Chip(
          labelStyle: chipTextStyle,
          label: Text('IN PROGRESS'),
          backgroundColor: AppColors.secondary400,
        );
      case TaskStatus.DONE:
        return Chip(
          labelStyle: chipTextStyle.copyWith(color: AppColors.whitePrimary),
          label: Text(
            'DONE',
          ),
          backgroundColor: AppColors.secondary,
        );
    }
  }
}
