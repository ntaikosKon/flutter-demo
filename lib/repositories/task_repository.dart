import '../models/models.dart';

abstract class TaskRepository {
  Future<List<Task>> getTasks();

  Future<Task> updateTaskById({
    required String id,
    String? description,
    String? title,
    TaskStatus? status,
    String? userId,
  });

  Future<Task> addNewTask(String title, String description);
}
