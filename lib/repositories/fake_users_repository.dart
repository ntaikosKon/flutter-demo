import 'package:uuid/uuid.dart';

import 'repositories.dart';
import '../models/models.dart';

List<User> _fakeUsers = [
  User(id: '1', name: 'name1', role: UserRole.DESIGNER),
  User(id: '2', name: 'name2', role: UserRole.DEVELOPER),
];

class FakeUsersRepository implements UsersRepository {
  late List<User> users;

  FakeUsersRepository() {
    users = [..._fakeUsers];
  }

  @override
  Future<List<User>> getUsers() {
    return Future.delayed(Duration(seconds: 4), () => users);
  }

  @override
  Future<User> addUser({required String name, required UserRole role}) {
    final newUser = User(id: Uuid().v1(), name: name, role: role);

    users = [...users]..add(newUser);

    return Future.delayed(Duration(seconds: 3), () => newUser);
  }

  @override
  Future<User> updateUser({required String id, String? name, UserRole? role}) {
    final updatedUser = users.firstWhere((user) => user.id == id).copyWith(
          name: name,
          role: role,
        );

    users = users
        .map(
          (user) => user.id == id ? updatedUser : user,
        )
        .toList();

    return Future.delayed(Duration(seconds: 1), () => updatedUser);
  }
}
