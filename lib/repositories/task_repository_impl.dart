import 'dart:convert';

import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../models/models.dart';
import '../providers/providers.dart';
import 'task_repository.dart';

final taskRepositoryProvider = Provider<TaskRepository>((ref) {
  return TaskRepositoryImpl(ref.read);
});

class TaskRepositoryImpl implements TaskRepository {
  final Reader read;

  TaskRepositoryImpl(this.read);

  @override
  Future<Task> addNewTask(String title, String description) async {
    try {
      List<Task> existingTasks = await _getTaskList();

      final newTask = Task(
        id: Uuid().v1(),
        title: title,
        description: description,
      );

      existingTasks = [...existingTasks]..add(newTask);

      await _saveTaskList(existingTasks);

      return newTask;
    } catch (e, s) {
      throw TaskExeption(
        message: 'Error while creating the new Task',
        stackTrace: s,
      );
    }
  }

  @override
  Future<List<Task>> getTasks() async {
    try {
      final tasks = await _getTaskList();
      return tasks;
    } catch (e, s) {
      throw TaskExeption(
        message: 'Error while fetchig existing tasks',
        stackTrace: s,
      );
    }
  }

  @override
  Future<Task> updateTaskById({
    required String id,
    String? description,
    String? title,
    TaskStatus? status,
    String? userId,
  }) async {
    try {
      List<Task> tasks = await _getTaskList();

      Task updatedTask = tasks.firstWhere((task) => task.id == id).copyWith(
            description: description,
            title: title,
            status: status,
            userId: userId,
          );

      tasks = tasks
          .map(
            (task) => task.id == id ? updatedTask : task,
          )
          .toList();

      await _saveTaskList(tasks);

      return updatedTask;
    } catch (e, s) {
      throw TaskExeption(
        message: 'Error while while updating tasks',
        stackTrace: s,
      );
    }
  }

  Future<List<Task>> _getTaskList() async {
    final prefs = await read(sharedPrefsProvider);

    final jsonList = prefs.getStringList('tasks') ?? [];

    return jsonList.map((task) => Task.fromMap(jsonDecode(task))).toList();
  }

  Future<void> _saveTaskList(List<Task> tasks) async {
    final prefs = await read(sharedPrefsProvider);

    final jsonList = tasks.map((task) => jsonEncode(task.toMap())).toList();

    prefs.setStringList('tasks', jsonList);
  }
}
