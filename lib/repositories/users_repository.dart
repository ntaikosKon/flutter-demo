import '../models/models.dart';

abstract class UsersRepository {
  Future<List<User>> getUsers();

  Future<User> addUser({
    required String name,
    required UserRole role,
  });

  Future<User> updateUser({
    required String id,
    String? name,
    UserRole? role,
  });
}
