import 'dart:convert';

import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../models/models.dart';
import '../providers/providers.dart';
import 'users_repository.dart';

final userRepositoryProvider = Provider<UsersRepository>((ref) {
  return UsersRepositoryImpl(ref.read);
});

class UsersRepositoryImpl implements UsersRepository {
  final Reader read;

  UsersRepositoryImpl(this.read);

  @override
  Future<User> addUser({required String name, required UserRole role}) async {
    try {
      List<User> existingUsers = await _getUsersList();

      final newUser = User(
        id: Uuid().v1(),
        name: name,
        role: role,
      );

      existingUsers = [...existingUsers]..add(newUser);

      await _saveUsersList(existingUsers);

      return newUser;
    } catch (e, s) {
      throw UserException(
        message: 'Error while creating the new User',
        stackTrace: s,
      );
    }
  }

  @override
  Future<List<User>> getUsers() async {
    try {
      final users = await _getUsersList();
      return users;
    } catch (e, s) {
      throw UserException(
        message: 'Error while fetchig existing users',
        stackTrace: s,
      );
    }
  }

  @override
  Future<User> updateUser(
      {required String id, String? name, UserRole? role}) async {
    try {
      List<User> users = await _getUsersList();

      final updatedUser = users.firstWhere((user) => user.id == id).copyWith(
            name: name,
            role: role,
          );

      users = users
          .map(
            (user) => user.id == id ? updatedUser : user,
          )
          .toList();

      await _saveUsersList(users);
      return updatedUser;
    } catch (e, s) {
      throw UserException(
        message: 'Error while updating the User',
        stackTrace: s,
      );
    }
  }

  Future<List<User>> _getUsersList() async {
    final prefs = await read(sharedPrefsProvider);

    final jsonList = prefs.getStringList('users') ?? [];

    return jsonList.map((user) => User.fromMap(jsonDecode(user))).toList();
  }

  Future<void> _saveUsersList(List<User> users) async {
    final prefs = await read(sharedPrefsProvider);

    final jsonList = users.map((user) => jsonEncode(user.toMap())).toList();

    prefs.setStringList('users', jsonList);
  }
}
