import 'package:uuid/uuid.dart';

import '../models/models.dart';
import './task_repository.dart';

List<Task> _fakeTasks = [
  Task(id: '1', title: 'title1', description: 'description1'),
  Task(
    id: '2',
    title: 'title2',
    description: 'description2',
    status: TaskStatus.IN_PROGRESS,
    userId: '1',
  ),
  Task(
    id: '3',
    title: 'title3',
    description: 'description3',
    status: TaskStatus.DONE,
    userId: '1',
  )
];

class FakeTasksRepository implements TaskRepository {
  late List<Task> tasks;

  FakeTasksRepository() {
    tasks = [..._fakeTasks];
  }

  @override
  Future<List<Task>> getTasks() {
    return Future.delayed(Duration(seconds: 1), () => tasks);
  }

  @override
  Future<Task> updateTaskById({
    required String id,
    String? description,
    String? title,
    TaskStatus? status,
    String? userId,
  }) {
    // return Future.delayed(
    //   Duration(seconds: 3),
    //   () => throw TaskExeption(message: 'Error while creating the task'),
    // );

    final updatedTask = tasks.firstWhere((task) => task.id == id).copyWith(
          description: description,
          title: title,
          status: status,
          userId: userId,
        );

    tasks = tasks
        .map(
          (task) => task.id == id ? updatedTask : task,
        )
        .toList();

    return Future.delayed(Duration(seconds: 1), () => updatedTask);
  }

  @override
  Future<Task> addNewTask(String title, String description) {
    final newTask =
        Task(id: Uuid().v1(), title: title, description: description);

    tasks = [...tasks]..add(newTask);

    return Future.delayed(Duration(seconds: 3), () => newTask);
  }
}
