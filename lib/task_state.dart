import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'models/models.dart';
import 'providers/providers.dart';
import 'repositories/repositories.dart';

final tasksStateProvider = StateNotifierProvider<TasksState>((ref) {
  return TasksState(ref.read);
});

final tasksByUserProvider =
    Provider.family<AsyncValue<List<Task>>, String>((ref, id) {
  final tasks = ref.watch(tasksStateProvider.state);

  return tasks
      .whenData((tasks) => tasks.where((task) => task.userId == id).toList());
});

final taskByIdProvider = Provider.family<AsyncValue<Task>, String>((ref, id) {
  final tasks = ref.watch(tasksStateProvider.state);

  return tasks.whenData((tasks) => tasks.firstWhere(((task) => task.id == id)));
});

class TasksState extends StateNotifier<AsyncValue<List<Task>>> {
  final Reader read;

  TasksState(
    this.read, [
    AsyncValue<List<Task>>? tasks,
  ]) : super(tasks ?? AsyncValue.loading()) {
    if (tasks == null) _getTasks();
  }

  void _getTasks() async {
    try {
      final tasks = await read(taskRepositoryProvider).getTasks();
      state = AsyncValue.data(tasks);
    } catch (e) {
      state = AsyncValue.error(e);
    }
  }

  Future<void> addTask({
    required String title,
    required String description,
  }) async {
    try {
      final newTask =
          await read(taskRepositoryProvider).addNewTask(title, description);

      state = state.whenData(
        (tasks) {
          return [...tasks]..add(newTask);
        },
      );
    } on TaskExeption catch (e) {
      _handleException(e);
    }
  }

  Future<void> updateTask({
    required String id,
    String? title,
    String? description,
    TaskStatus? status,
    String? userId,
  }) async {
    try {
      final updatedTask = await read(taskRepositoryProvider).updateTaskById(
        id: id,
        title: title,
        status: status,
        description: description,
        userId: userId,
      );

      state = state.whenData(
        (value) => value.map((task) {
          if (task.id == id) {
            return updatedTask;
          }
          return task;
        }).toList(),
      );
    } on TaskExeption catch (e) {
      _handleException(e);
    }
  }

  void _handleException(TaskExeption e) {
    read(exceptionProvider).state = e;
  }
}
