import 'package:flutter_test/flutter_test.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'package:demo/task_state.dart';
import 'package:demo/repositories/repositories.dart';
import 'package:demo/models/models.dart';
import 'package:demo/providers/providers.dart';
import 'task_state_test.mocks.dart';

@GenerateMocks([TaskRepository])
void main() {
  late MockTaskRepository mockTaskRepository;

  setUp(() {
    mockTaskRepository = MockTaskRepository();
  });

  tearDown(() {
    reset(mockTaskRepository);
  });
  group('tasksStateProvider', () {
    test('initialy emits AsyncValue.loading()', () {
      when(mockTaskRepository.getTasks()).thenAnswer((_) async => []);

      final container = ProviderContainer(overrides: [
        taskRepositoryProvider.overrideWithValue(mockTaskRepository),
      ]);

      addTearDown(container.dispose);

      expect(
        container.read(tasksStateProvider.state),
        AsyncValue<List<Task>>.loading(),
      );
    });

    test('when created calls call the repository to load tasks', () {
      final container = ProviderContainer(overrides: [
        taskRepositoryProvider.overrideWithValue(mockTaskRepository),
      ]);

      addTearDown(container.dispose);

      container.read(tasksStateProvider.state);

      verify(mockTaskRepository.getTasks());
    });

    test('loads the tasks fm repository as AsyncValue.data()', () async {
      when(mockTaskRepository.getTasks()).thenAnswer(
        (_) async => [
          Task(id: 'id', title: 'title', description: 'description'),
        ],
      );

      final container = ProviderContainer(overrides: [
        taskRepositoryProvider.overrideWithValue(mockTaskRepository),
      ]);

      addTearDown(container.dispose);

      container.read(tasksStateProvider.state);

      await untilCalled(mockTaskRepository.getTasks());

      expect(
        container.read(tasksStateProvider.state).data!.value.length,
        1,
      );

      expect(
        container.read(tasksStateProvider.state).data!.value[0],
        isA<Task>()
            .having((t) => t.id, 'id', 'id')
            .having((t) => t.title, 'title', 'title')
            .having((t) => t.description, 'description', 'description'),
      );
    });

    test('emits an AsyncValue.error() if the repository throws an exeption',
        () async {
      when(mockTaskRepository.getTasks()).thenThrow(
        TaskExeption,
      );

      final container = ProviderContainer(overrides: [
        taskRepositoryProvider.overrideWithValue(mockTaskRepository),
      ]);

      addTearDown(container.dispose);

      container.read(tasksStateProvider.state);

      await untilCalled(mockTaskRepository.getTasks());

      expect(
        container.read(tasksStateProvider.state),
        AsyncValue<List<Task>>.error(TaskExeption, null),
      );
    });

    group('when adding a new task', () {
      late ProviderContainer container;

      setUp(() {
        container = ProviderContainer(overrides: [
          taskRepositoryProvider.overrideWithValue(mockTaskRepository),
          tasksStateProvider.overrideWithProvider(
            StateNotifierProvider<TasksState>(
              (ref) {
                return TasksState(
                  ref.read,
                  AsyncValue.data(
                    [
                      Task(id: 'id', title: 'title', description: 'description')
                    ],
                  ),
                );
              },
            ),
          )
        ]);
      });

      test('calls the repository to persist the new task', () {
        when(
          mockTaskRepository.addNewTask('title2', 'description2'),
        ).thenAnswer(
          (_) async => Task(
            id: 'id2',
            title: 'title2',
            description: 'description2',
          ),
        );

        addTearDown(container.dispose);

        container
            .read(tasksStateProvider)
            .addTask(title: 'title2', description: 'description2');

        verify(mockTaskRepository.addNewTask('title2', 'description2'));
      });

      test('waits the repository to return the new task before updating', () {
        addTearDown(container.dispose);

        when(
          mockTaskRepository.addNewTask('title2', 'description2'),
        ).thenAnswer(
          (_) async => Task(
            id: 'id2',
            title: 'title2',
            description: 'description2',
          ),
        );

        container
            .read(tasksStateProvider)
            .addTask(title: 'title2', description: 'description2');

        expect(
          container.read(tasksStateProvider.state).data!.value.length,
          1,
        );
      });

      test('updates the state when the repository responces', () async {
        addTearDown(container.dispose);

        when(
          mockTaskRepository.addNewTask('title2', 'description2'),
        ).thenAnswer(
          (_) async => Task(
            id: 'id2',
            title: 'title2',
            description: 'description2',
          ),
        );

        container
            .read(tasksStateProvider)
            .addTask(title: 'title2', description: 'description2');

        await untilCalled(
            mockTaskRepository.addNewTask('title2', 'description2'));

        expect(
          container.read(tasksStateProvider.state).data!.value.length,
          2,
        );

        expect(
          container.read(tasksStateProvider.state).data!.value[1],
          isA<Task>()
              .having((t) => t.id, 'id', 'id2')
              .having((t) => t.title, 'title', 'title2')
              .having((t) => t.description, 'description', 'description2'),
        );
      });

      test('keeps the previeus state if the repository throws an error',
          () async {
        addTearDown(container.dispose);

        when(
          mockTaskRepository.addNewTask('title2', 'description2'),
        ).thenThrow(TaskExeption(message: 'Error'));

        container
            .read(tasksStateProvider)
            .addTask(title: 'title2', description: 'description2');

        await untilCalled(
            mockTaskRepository.addNewTask('title2', 'description2'));

        expect(
          container.read(tasksStateProvider.state).data!.value.length,
          1,
        );
      });

      test(
          'emits the error to the taskExceptionProvider if the repository throws an error',
          () async {
        addTearDown(container.dispose);

        when(
          mockTaskRepository.addNewTask('title2', 'description2'),
        ).thenThrow(TaskExeption(message: 'Error while adding the task'));

        container
            .read(tasksStateProvider)
            .addTask(title: 'title2', description: 'description2');

        await untilCalled(
            mockTaskRepository.addNewTask('title2', 'description2'));

        expect(
          container.read(exceptionProvider).state,
          isA<TaskExeption>().having(
              (e) => e.message, 'message', 'Error while adding the task'),
        );
      });
    });

    group('When updating an existing task', () {
      late ProviderContainer container;

      setUp(() {
        container = ProviderContainer(overrides: [
          taskRepositoryProvider.overrideWithValue(mockTaskRepository),
          tasksStateProvider.overrideWithProvider(
            StateNotifierProvider<TasksState>(
              (ref) {
                return TasksState(
                  ref.read,
                  AsyncValue.data(
                    [
                      Task(id: 'id', title: 'title', description: 'description')
                    ],
                  ),
                );
              },
            ),
          )
        ]);
      });

      void stubTaskRepository() {
        when(
          mockTaskRepository.updateTaskById(
            id: 'id',
            title: 'newTitle',
            description: null,
            status: null,
            userId: null,
          ),
        ).thenAnswer(
          (_) async => Task(
            id: 'id',
            title: 'newTitle',
            description: 'description',
          ),
        );
      }

      void stubTaskRepositoryExeption() {
        when(
          mockTaskRepository.updateTaskById(
            id: 'id',
            title: 'newTitle',
            description: null,
            status: null,
            userId: null,
          ),
        ).thenThrow(TaskExeption(message: 'Error while updating task'));
      }

      test('initialy calls the repository to persist the update', () {
        addTearDown(container.dispose);
        stubTaskRepository();

        container
            .read(tasksStateProvider)
            .updateTask(id: 'id', title: 'newTitle');

        verify(mockTaskRepository.updateTaskById(
          id: 'id',
          title: 'newTitle',
          description: null,
          status: null,
          userId: null,
        ));
      });

      test('waits the repository to return the new task before updating', () {
        addTearDown(container.dispose);
        stubTaskRepository();

        container
            .read(tasksStateProvider)
            .updateTask(id: 'id', title: 'newTitle');

        expect(
          container.read(tasksStateProvider.state).data!.value[0],
          isA<Task>()
              .having((t) => t.id, 'id', 'id')
              .having((t) => t.title, 'title', 'title')
              .having((t) => t.description, 'description', 'description'),
        );
      });

      test('updated the state wnen the repository responces', () async {
        addTearDown(container.dispose);
        stubTaskRepository();

        container
            .read(tasksStateProvider)
            .updateTask(id: 'id', title: 'newTitle');

        await untilCalled(
          mockTaskRepository.updateTaskById(
            id: 'id',
            title: 'newTitle',
            description: null,
            status: null,
            userId: null,
          ),
        );

        expect(
          container.read(tasksStateProvider.state).data!.value[0],
          isA<Task>()
              .having((t) => t.id, 'id', 'id')
              .having((t) => t.title, 'title', 'newTitle')
              .having((t) => t.description, 'description', 'description'),
        );
      });

      test('keeps the previeus state if the repository throws an error',
          () async {
        addTearDown(container.dispose);
        stubTaskRepositoryExeption();

        container
            .read(tasksStateProvider)
            .updateTask(id: 'id', title: 'newTitle');

        await untilCalled(
          mockTaskRepository.updateTaskById(
            id: 'id',
            title: 'newTitle',
            description: null,
            status: null,
            userId: null,
          ),
        );

        expect(
          container.read(tasksStateProvider.state).data!.value[0],
          isA<Task>()
              .having((t) => t.id, 'id', 'id')
              .having((t) => t.title, 'title', 'title')
              .having((t) => t.description, 'description', 'description'),
        );
      });

      test(
          'emits the error to the taskExceptionProvider if the repository throws an error',
          () async {
        addTearDown(container.dispose);
        stubTaskRepositoryExeption();

        container
            .read(tasksStateProvider)
            .updateTask(id: 'id', title: 'newTitle');

        await untilCalled(
          mockTaskRepository.updateTaskById(
            id: 'id',
            title: 'newTitle',
            description: null,
            status: null,
            userId: null,
          ),
        );

        expect(
          container.read(exceptionProvider).state,
          isA<TaskExeption>()
              .having((e) => e.message, 'message', 'Error while updating task'),
        );
      });
    });
  });
}
