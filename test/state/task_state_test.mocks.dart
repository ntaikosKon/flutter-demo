// Mocks generated by Mockito 5.0.1 from annotations
// in demo/test/task_state_test.dart.
// Do not manually edit this file.

import 'dart:async' as _i4;

import 'package:demo/models/task.dart' as _i2;
import 'package:demo/repositories/task_repository.dart' as _i3;
import 'package:mockito/mockito.dart' as _i1;

// ignore_for_file: comment_references
// ignore_for_file: unnecessary_parenthesis

class _FakeTask extends _i1.Fake implements _i2.Task {}

/// A class which mocks [TaskRepository].
///
/// See the documentation for Mockito's code generation for more information.
class MockTaskRepository extends _i1.Mock implements _i3.TaskRepository {
  MockTaskRepository() {
    _i1.throwOnMissingStub(this);
  }

  @override
  _i4.Future<List<_i2.Task>> getTasks() => (super.noSuchMethod(
      Invocation.method(#getTasks, []),
      returnValue: Future.value(<_i2.Task>[])) as _i4.Future<List<_i2.Task>>);
  @override
  _i4.Future<_i2.Task> updateTaskById(
          {String? id,
          String? description,
          String? title,
          _i2.TaskStatus? status,
          String? userId}) =>
      (super.noSuchMethod(
          Invocation.method(#updateTaskById, [], {
            #id: id,
            #description: description,
            #title: title,
            #status: status,
            #userId: userId
          }),
          returnValue: Future.value(_FakeTask())) as _i4.Future<_i2.Task>);
  @override
  _i4.Future<_i2.Task> addNewTask(String? title, String? description) =>
      (super.noSuchMethod(Invocation.method(#addNewTask, [title, description]),
          returnValue: Future.value(_FakeTask())) as _i4.Future<_i2.Task>);
}
