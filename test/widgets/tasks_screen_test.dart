import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'package:demo/task_state.dart';
import 'package:demo/screens/tasks_screen.dart';
import 'package:demo/models/task.dart';

class _TestApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: TasksScreen(),
    );
  }
}

List<Task> _tasks = [
  Task(
      id: 'id1',
      title: 'to_do_title',
      description: 'description1',
      status: TaskStatus.TO_DO),
  Task(
      id: 'id2',
      title: 'in_progress_title',
      description: 'description2',
      status: TaskStatus.IN_PROGRESS),
  Task(
      id: 'id3',
      title: 'done_title',
      description: 'description3',
      status: TaskStatus.DONE),
];

void main() {
  getTestedEnv(AsyncValue<List<Task>> tasksState) {
    return ProviderScope(
      overrides: [tasksStateProvider.state.overrideWithValue(tasksState)],
      child: _TestApp(),
    );
  }

  group('tasks screen widget', () {
    testWidgets('shows a loading spiner when the state is loading',
        (tester) async {
      await tester.pumpWidget(
        getTestedEnv(
          AsyncValue.loading(),
        ),
      );

      expect(find.byType(CircularProgressIndicator), findsOneWidget);
    });

    testWidgets('shows an error text if the state is error', (tester) async {
      await tester.pumpWidget(
        getTestedEnv(
          AsyncValue.error(Error()),
        ),
      );

      expect(find.text('Error while loading tasks'), findsOneWidget);
    });

    group('When the task state contains tasks', () {
      testWidgets('initialy displays all tasks', (tester) async {
        await tester.pumpWidget(
          getTestedEnv(
            AsyncValue.data(_tasks),
          ),
        );

        expect(find.text('to_do_title'), findsOneWidget);
        expect(find.text('in_progress_title'), findsOneWidget);
        expect(find.text('done_title'), findsOneWidget);
      });

      testWidgets('displays only the to_do tasks if the filter is selected',
          (tester) async {
        await tester.pumpWidget(
          getTestedEnv(
            AsyncValue.data(_tasks),
          ),
        );

        //Open the filter menu
        await tester.tap(find.byKey(Key('menu-button')));
        await tester.pumpAndSettle();

        //select the DO TO option
        await tester.tap(find.byKey(Key('menu-status-toDo')));
        await tester.pump();

        //verify that only the TO_DO titles are presented
        expect(find.text('to_do_title'), findsOneWidget);
        expect(find.text('in_progress_title'), findsNothing);
        expect(find.text('done_title'), findsNothing);
      });

      testWidgets(
          'displays only the in_progress tasks if the filter is selected',
          (tester) async {
        await tester.pumpWidget(
          getTestedEnv(
            AsyncValue.data(_tasks),
          ),
        );

        //Open the filter menu
        await tester.tap(find.byKey(Key('menu-button')));
        await tester.pumpAndSettle();

        //select the IN PROGRESS option
        await tester.tap(find.byKey(Key('menu-status-inProgress')));
        await tester.pump();

        //verify that only the IN_PROGRESS titles are presented
        expect(find.text('to_do_title'), findsNothing);
        expect(find.text('in_progress_title'), findsOneWidget);
        expect(find.text('done_title'), findsNothing);
      });
    });
  });
}
