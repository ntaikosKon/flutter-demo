import 'package:flutter_test/flutter_test.dart';

import 'package:demo/models/models.dart';

void main() {
  group('When creating a task', () {
    test('adds default value for the status if not provided', () {
      final task = Task(
        id: 'id',
        title: 'title',
        description: 'description',
      );

      expect(task.id, 'id');
      expect(task.description, 'description');
      expect(task.title, 'title');
      expect(task.userId, null);
      expect(task.status, TaskStatus.TO_DO);
    });

    test('adds the provided status if provided', () {
      final task = Task(
        id: 'id',
        title: 'title',
        description: 'description',
        status: TaskStatus.IN_PROGRESS,
      );

      expect(task.userId, null);
      expect(task.status, TaskStatus.IN_PROGRESS);
    });
  });

  group('When coping from an existing task', () {
    test('does not mutate the existing', () {
      final task = Task(
        id: 'id',
        title: 'title',
        description: 'description',
      );

      task.copyWith(status: TaskStatus.IN_PROGRESS);

      expect(task.status, TaskStatus.TO_DO);
    });

    test('creates a new instance overiding the provided properties', () {
      final task = Task(
        id: 'id',
        title: 'title',
        description: 'description',
      );

      final newTask = task.copyWith(
        userId: 'userId',
        status: TaskStatus.IN_PROGRESS,
      );

      expect(newTask.id, 'id');
      expect(newTask.description, 'description');
      expect(newTask.title, 'title');
      expect(newTask.status, TaskStatus.IN_PROGRESS);
      expect(newTask.userId, 'userId');
    });
  });

  group('when creating a task from a Map', () {
    test(
        'throws an exeption if the Map does not contain all the required field',
        () {
      Map<String, dynamic> map = {
        'id': 'id',
        'title': 'title',
      };

      expect(
        () => Task.fromMap(map),
        throwsA(
          predicate((e) => e is TaskExeption),
        ),
      );
    });

    test('throws an exeption if the Map contains an invalid status index', () {
      Map<String, dynamic> map = {
        'id': 'id',
        'title': 'title',
        'description': 'description',
        'status': 5
      };

      expect(
        () => Task.fromMap(map),
        throwsA(
          predicate((e) => e is TaskExeption),
        ),
      );
    });

    test('it is created of the Map contains all required fields', () {
      Map<String, dynamic> map = {
        'id': 'id',
        'title': 'title',
        'description': 'description',
        'status': 1
      };

      late Task task;

      expect(() {
        task = Task.fromMap(map);
      }, returnsNormally);

      expect(task.id, 'id');
      expect(task.title, 'title');
      expect(task.description, 'description');
      expect(task.status, TaskStatus.IN_PROGRESS);
      expect(task.userId, null);
    });
  });

  test('converts to a Task to Map', () {
    final task = Task(
      id: 'id',
      title: 'title',
      description: 'description',
      status: TaskStatus.DONE,
      userId: 'userId',
    );

    final map = task.toMap();

    expect(map, isA<Map<String, dynamic>>());

    expect(map['id'], 'id');
    expect(map['title'], 'title');
    expect(map['description'], 'description');
    expect(map['status'], TaskStatus.DONE.index);
    expect(map['userId'], 'userId');
  });
}
