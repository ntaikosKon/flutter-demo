#Flutter Demo Project

This is a **Task Management App** project that aims to demonstrate some Flutter related concepts.

### App Description
The App provides the option to create and update Tasks that are saved to the Mobile storage. In addition the App User has the ability to change the status of the Task (TO_DO, IN_PROGRESS, DONE). The App provides the meens to add new Users or update exiting. Each User can be assigned a role (Developer or Designer). The App User can assign to each Task a User. The App has view to present Tasks assigned to a specific User and view to present the assigned User to a Task.

### What this project demonstrates?
- Implementation of a custom design (in this depo we implement a neuromorphic based design)
- How to share decorations and styles accross widgets
- Creating custom shapes using Clip Paths
- Creating custom complex annimations
- Creating custom route transitions
- Organaizing the State Management of an app in multiple layers
- Selecting portions of the App State in each screen
- Handling exeptions and showing descriptive toast messages in a centralized place
- Writing Unit tests

### Libraries
- [Riverpod](https://riverpod.dev/ "Riverpod") (with Hooks) for the app state management
- [Flutter Hooks](https://pub.dev/packages/flutter_hooks "Flutter Hooks") for widget state management
- [mockito](https://pub.dev/packages/mockito "mockito") for mocking and stubing during tests
- [shared_preferences](https://pub.dev/packages/shared_preferences "shared_preferences") for persisting data to the mobile devise
- [modal_bottom_sheet](https://pub.dev/packages/modal_bottom_sheet "modal_bottom_sheet")
- [fluttertoast](https://pub.dev/packages/fluttertoast/versions/8.0.1-nullsafety.0 "fluttertoast")
